#!/bin/bash
# Install prerequisite packages
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common gnupg-agent wget openssl
# Add Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Install Docker and Docker Compose
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
sudo systemctl enable docker

# Install Docker Compose
sudo curl -fsSL https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Create directories for ownCloud and related services
mkdir -p /home/adminuser/docker
cd ~/docker

#traefik
sudo mkdir /etc/traefik
sudo wget -O /home/adminuser/docker/dynamic_conf.yml https://gitlab.com/s.denbol/owncloud/-/raw/main/dynamic_conf.yml 
sudo cp -a /home/adminuser/docker/dynamic_conf.yml /etc/traefik/

# Download necessary configuration files 
sudo wget -O /home/adminuser/docker/docker-compose.yml https://gitlab.com/s.denbol/owncloud/-/raw/main/docker-compose.yml && sudo wget -O /home/adminuser/docker/.env https://gitlab.com/s.denbol/owncloud/-/raw/main/.env

# Change permissions of docker-compose.yml
sudo chmod +x /home/adminuser/docker/docker-compose.yml

# Prepare directories for services (ownCloud)
sudo mkdir -p /var/lib/mysql /etc/mysql/conf.d/ /var/www/html

# Generate SSL certificate
sudo mkdir -p /certs
sudo openssl req -x509 -newkey rsa:4096 -nodes -keyout /certs/private.key -out /certs/certificate.crt -days 365 -subj "/CN=scamcloud.nl"

# Move necessary files to their expected locations
#mv docker-compose.yml ~/owncloud/docker/
#mv .env /home/adminuser/docker/

# Start the ownCloud services using Docker Compose
sleep 30
sudo docker-compose -f ~/docker/docker-compose.yml up -d

# Configure firewall rules
sudo ufw enable
declare -a ports=("22/tcp" "9080/tcp" "9443/tcp")
for port in "${ports[@]}"; do
    sudo ufw allow "$port"
done
sudo ufw reload
