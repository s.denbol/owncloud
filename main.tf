terraform {
  required_version = ">=0.12"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~>3.0"
    }
  }
}

#configure provider
provider "azurerm" {
  skip_provider_registration = "true"
  features {}
}
#configure resource group
resource "azurerm_resource_group" "ResourceGroup1" {
  name     = "RG1"
  location = "West Europe"

}

#configure virtual network 1 
resource "azurerm_virtual_network" "VN1" {
  name                = "Network1"
  resource_group_name = azurerm_resource_group.ResourceGroup1.name
  location            = azurerm_resource_group.ResourceGroup1.location
  address_space       = ["10.10.0.0/16"]

  tags = {
    environment = "dev"
  }
}

#configure subnet 1
resource "azurerm_subnet" "subnet1" {
  name                 = "subnet1"
  resource_group_name  = azurerm_resource_group.ResourceGroup1.name
  virtual_network_name = azurerm_virtual_network.VN1.name
  address_prefixes     = ["10.10.1.0/24"]

}

#configure security group + rules for dev
resource "azurerm_network_security_group" "SG1" {
  name                = "SecurityGroup1"
  location            = azurerm_resource_group.ResourceGroup1.location
  resource_group_name = azurerm_resource_group.ResourceGroup1.name

  #define what the dev is allowed to access/which protocols are allowed to be used from outside.
  security_rule {
    name                       = "dev-rule"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "dev"
  }
}

#adding security rule to resource group 1 
resource "azurerm_subnet_network_security_group_association" "SGA1" {
  subnet_id                 = azurerm_subnet.subnet1.id
  network_security_group_id = azurerm_network_security_group.SG1.id
}

#config for Public IP-address 1
resource "azurerm_public_ip" "IP1" {
  name                = "PublicIP1"
  resource_group_name = azurerm_resource_group.ResourceGroup1.name
  location            = azurerm_resource_group.ResourceGroup1.location
  allocation_method   = "Dynamic"

  tags = {
    environment = "dev"
  }
}

#config for Network Interface Card1
resource "azurerm_network_interface" "NIC1" {
  name                = "NetworkInterfaceCard1"
  location            = azurerm_resource_group.ResourceGroup1.location
  resource_group_name = azurerm_resource_group.ResourceGroup1.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.IP1.id
  }

  tags = {
    environment = "dev"
  }
}

# Config for Storage Account
resource "azurerm_storage_account" "SA1" {
  name                     = "imnotgonnasugarcoatit1" #required to use lowercase and must be unique, otherwise terraform will get angry.
  resource_group_name      = azurerm_resource_group.ResourceGroup1.name
  location                 = azurerm_resource_group.ResourceGroup1.location
  account_tier             = "Standard"
  account_replication_type = "LRS" # Specify the replication type
}

# Create Log Analytics workspace
resource "azurerm_log_analytics_workspace" "LA1" {
  name                = "LogAnalytics1"
  location            = azurerm_resource_group.ResourceGroup1.location
  resource_group_name = azurerm_resource_group.ResourceGroup1.name
  sku                 = "PerGB2018" # Specify the Log Analytics workspace SKU, free is good enough, but not allowed
  retention_in_days   = 30          # Set the data retention period, the free tier would've been better...
}

#config the Linux Virtual Machine numero uno
resource "azurerm_linux_virtual_machine" "LM1" {
  name                  = "LinuxMachine1"
  resource_group_name   = azurerm_resource_group.ResourceGroup1.name
  location              = azurerm_resource_group.ResourceGroup1.location
  size                  = "Standard_B2s"
  admin_username        = "adminuser"
  network_interface_ids = [azurerm_network_interface.NIC1.id, ]

  custom_data = filebase64("customdata.tpl")

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/azurekey.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    offer     = "0001-com-ubuntu-server-focal"
    publisher = "Canonical"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
}


# # Config for Azure Monitor
# resource "azurerm_monitor_diagnostic_setting" "MS1" {
#   name                       = "MonitorSettings1"
#   target_resource_id         = azurerm_linux_virtual_machine.LM1.id
#   storage_account_id         = azurerm_storage_account.SA1.id
#   log_analytics_workspace_id = azurerm_log_analytics_workspace.LA1.id

#   #  log {
#   #    category = "AllLogs" # Collect all logs for the resource
#   #    enabled  = true
#   #  }

#   metric {
#     category = "AllMetrics" # Collect capacity-related metrics
#     enabled  = true
#   }
# }

# Config for Action Group
resource "azurerm_monitor_action_group" "AG1" {
  name                = "AlertActionGroup1"
  resource_group_name = azurerm_resource_group.ResourceGroup1.name

  short_name = "HighCPU"
  #location     = azurerm_resource_group.ResourceGroup1.location
  enabled = true

  email_receiver {
    name                    = "EmailReceiver"
    email_address           = "sfrndb@gmail.com"
    use_common_alert_schema = true
  }
}

# Config for Metric Alert 
resource "azurerm_monitor_metric_alert" "MA1" {
  name                = "MetricAlert1"
  resource_group_name = azurerm_resource_group.ResourceGroup1.name

  scopes = [azurerm_linux_virtual_machine.LM1.id]

  criteria {
    metric_namespace = "Microsoft.Compute/virtualMachines"
    metric_name      = "Percentage CPU"
    aggregation      = "Maximum"
    operator         = "GreaterThan"
    threshold        = 60
  }

  action {
    action_group_id = azurerm_monitor_action_group.AG1.id
  }
}


#output Ip-address of LM1
data "azurerm_public_ip" "INF1" {
  name                = azurerm_public_ip.IP1.name
  resource_group_name = azurerm_resource_group.ResourceGroup1.name
}
output "public_ip_address" {
  value = "${azurerm_linux_virtual_machine.LM1.name}: ${data.azurerm_public_ip.INF1.ip_address}"
}

